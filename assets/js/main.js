
const data = [
    {
        title: 'عسكري',
        body: 'UAR 2005 "Album LA GUARDIA"',
        audioSrc: 'assets/media/sounds/uar-album-la-guardia-6.mp3'
    },
    {
        title: 'سبابي قلبي',
        body: 'UAR 2005 "Album LA GUARDIA"',
        audioSrc: 'assets/media/sounds/uar-album-la-guardia-7.mp3'
    },

    {
        title: 'OUTRO - ONLY ASFAR',
        body: 'UAR 2005 "Album LA GUARDIA"',
        audioSrc: 'assets/media/sounds/uar-album-la-guardia-8.mp3'
    }
];


const titleSong = document.querySelector('.song .title');
const bodySong = document.querySelector('.song .body');
const AudioSong = document.querySelector('.song .audio');


const playlist = document.querySelector('.play-list');
const playlistItems = playlist.querySelector('ul');
const hamburgerMenu = document.querySelector('.hamburger-menu');
const hamburgerMenuIcon = hamburgerMenu.querySelector('i');


const prevSong = document.querySelector('.prev-song');
const playPause = document.querySelector('.play-pause');
const playPauseIcon = playPause.querySelector('i');
const nextSong = document.querySelector('.next-song');
const progressBar = document.querySelector('.progress-bar');

let currentSongIndex = 0;


function loadSong(index) {
    const currentSong = data[index];
    titleSong.textContent = currentSong.title;
    bodySong.textContent = currentSong.body;
    AudioSong.src = currentSong.audioSrc;
}


playPause.addEventListener('click', () => {
    if (playPauseIcon.classList.contains('fa-pause')) {
        AudioSong.pause();
        playPauseIcon.classList.remove('fa-pause');
        playPauseIcon.classList.add('fa-play');
    } else {
        AudioSong.play();
        playPauseIcon.classList.add('fa-pause');
        playPauseIcon.classList.remove('fa-play');
    }
});


nextSong.addEventListener('click', () => {
    currentSongIndex = (currentSongIndex + 1) % data.length;
    loadSong(currentSongIndex);

    playPauseIcon.classList.add('fa-pause');
    playPauseIcon.classList.remove('fa-play');
    AudioSong.play();
});


prevSong.addEventListener('click', () => {
    currentSongIndex = (currentSongIndex - 1 + data.length) % data.length;
    loadSong(currentSongIndex);

    playPauseIcon.classList.add('fa-pause');
    playPauseIcon.classList.remove('fa-play');
    AudioSong.play();
});


AudioSong.onloadedmetadata = function () {
    progressBar.max = AudioSong.duration;
    progressBar.value = AudioSong.currentTime;
}

AudioSong.addEventListener('timeupdate', function () {
    progressBar.value = AudioSong.currentTime;
});

progressBar.oninput = function () {
    AudioSong.currentTime = progressBar.value;
};

// Load the first song on page load
loadSong(currentSongIndex);

function selectSong(index) {
    currentSongIndex = index;
    loadSong(currentSongIndex);
    AudioSong.play();

    playPauseIcon.classList.add('fa-pause');
    playPauseIcon.classList.remove('fa-play');
}


// Function to load and update the playlist
function loadPlaylist() {
    playlistItems.innerHTML = '';

    data.forEach((item, index) => {
        const listItem = document.createElement('li');

        // Create an icon element
        const icon = document.createElement('i');
        icon.classList.add('fa', 'fa-music');
        listItem.appendChild(icon);


        const titleSpan = document.createElement('span');
        titleSpan.textContent = item.title;
        listItem.appendChild(titleSpan);

        listItem.addEventListener('click', () => {
            selectSong(index)
        });

        playlistItems.appendChild(listItem);

    });
}



// Load the playlist on page load
loadPlaylist();


hamburgerMenu.addEventListener("click", function () {
    playlist.classList.toggle("-is-opend");

    if (playlist.classList.contains('-is-opend')) {
        hamburgerMenuIcon.classList.add("fa-xmark");
        hamburgerMenuIcon.classList.remove("fa-bars");
    }
    else {
        hamburgerMenuIcon.classList.remove("fa-xmark");
        hamburgerMenuIcon.classList.add("fa-bars");
    }
})